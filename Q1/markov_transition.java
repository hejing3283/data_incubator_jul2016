
public class markov_transition {
    public static double get_matrix(){
    	int crnt_pos = 0;
    	int crnt_val = 0;
    	int line;
    	int next_pos = 0;
    	int next_val = 0;
    	int row;
    	double sum =0;
    	double expectation = 0;
    	double diviation = 0;
    	double diviation_sqr = 0;
    	double markov_matrix[][] = new double[100][100];
    	double init_vector[] = new double[100];
    	double next_vector[] = new double[100];
    	double crnt_vector[] = new double[100];
    	double s_mod_vector[] = new double[10];
    	double crnt_rate =0;
    //*************************************
    //initializing vector;
    //*************************************
    	for(int i=0;i<100;i++) {
    		if(i==0) {
    			init_vector[i] = 1;
    		} else {
    			init_vector[i] = 0;
    		}
    		crnt_vector[i] = init_vector[i];
    		next_vector[i] = 0;
    	}
    	for(int i=0;i<10;i++) {
    		s_mod_vector[i] = 0;
    	}
    //*************************************
    //initializing markov_matrix ;
    //*************************************
    	for(int i=0;i<100;i++) {
    		for(int j=0;j<100;j++){
    			markov_matrix[i][j] =0;
    		}
    	}
    //*************************************
    //build markov_matrix;
    //*************************************
    	for(int i=0;i<10;i++){
    		for(int j=0;j<10;j++){
    			crnt_pos = i;
    			crnt_val = j;
                //locate current position & value
    			line = crnt_val *10 +crnt_pos;
                //locate next position & value (may be multiple)
    			switch (crnt_pos) {
                  //from 0, transit to 4 or 6 with 1/2 chance
    			  case 0:
    			    next_pos = 4;
    			    next_val = (crnt_val + 4) % 10;
    			    row = next_val *10 + next_pos;
    			    markov_matrix[line][row] = 0.5;
    			    
    			    next_pos = 6;
    			    next_val = (crnt_val +6) % 10;
    			    row = next_val *10 + next_pos;
    			    markov_matrix[line][row] = 0.5;
    			    break;
    			  //from 1, transit to 6 or 8 with 1/2 chance
    			  case 1:
    				next_pos = 6;
      			    next_val = (crnt_val + 6) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 8;
      			    next_val = (crnt_val +8) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
      			  //from 2, transit to 7 or 9 with 1/2 chance
    			  case 2:
    				next_pos = 7;
      			    next_val = (crnt_val + 7) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 9;
      			    next_val = (crnt_val +9) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
      			    
      			  //from 3, transit to 4 or 8 with 1/2 chance
    			  case 3:
    				next_pos = 4;
      			    next_val = (crnt_val + 4) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 8;
      			    next_val = (crnt_val +8) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
      		  	  //from 4, transit to 3,9 or 0 with 1/3 chance
    			  case 4:
    				next_pos = 3;
      			    next_val = (crnt_val + 3) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.3333333333;

      			    next_pos = 9;
    			    next_val = (crnt_val + 9) % 10;
    			    row = next_val *10 + next_pos;
    			    markov_matrix[line][row] = 0.3333333333;
    			    
    			    next_pos = 0;
      			    next_val = (crnt_val +0) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.3333333333;
      			    break;
      			  //from 5, skip
    			  case 5:
    				break;
      			  //from 6, transit to 1,7 or 0 with 1/3 chance
    			  case 6:
    				next_pos = 1;
      			    next_val = (crnt_val + 1) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.3333333333;

      			    next_pos = 7;
    			    next_val = (crnt_val + 7) % 10;
    			    row = next_val *10 + next_pos;
    			    markov_matrix[line][row] = 0.3333333333;
    			    
    			    next_pos = 0;
      			    next_val = (crnt_val +0) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.3333333333;
      			    break;
      			  //from 7, transit to 2 or 6 with 1/2 chance
    			  case 7:
    				next_pos = 2;
      			    next_val = (crnt_val + 2) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 6;
      			    next_val = (crnt_val +6) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
      			    
      			//from 8, transit to 1 or 3 with 1/2 chance
    			  case 8:
    				next_pos = 1;
      			    next_val = (crnt_val + 1) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 3;
      			    next_val = (crnt_val +3) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
      			//from 9, transit to 2 or 4 with 1/2 chance
    			  case 9:
    				next_pos = 2;
      			    next_val = (crnt_val + 2) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    
      			    next_pos = 4;
      			    next_val = (crnt_val +4) % 10;
      			    row = next_val *10 + next_pos;
      			    markov_matrix[line][row] = 0.5;
      			    break;
    			}
    		}
    	}

    	//*************************************
    	//calculating the next vector;
    	//*************************************
    	//for the k move
    	for(int k=0;k<10;k++){
    		//calculate next vector for each move
    		for(int i=0;i<100;i++) {
    			sum =0;
    			for(int j=0;j<100;j++){
    				sum += crnt_vector[j] * markov_matrix[j][i];
    			}
    			next_vector[i] = sum;
    		}
    		//update next vector
    		for(int i=0;i<100;i++) {
    			crnt_vector[i] = next_vector[i];
    		}
    	}
    	
    	//*************************************
    	//using the final vector to calculate the expectation
    	//*************************************
    	for(int i=0;i<100;i++) {
			crnt_val = i/10;
			s_mod_vector[crnt_val] += crnt_vector[i];			
		}
    	for(int i=0;i<10;i++) {
    		expectation += s_mod_vector[i]*i;
    		System.out.println(i+":"+s_mod_vector[i]);
    	}
    	for(int i=0;i<10;i++) {
    		diviation_sqr += (i -expectation)*(i-expectation)*s_mod_vector[i];
    	}
    	diviation = Math.sqrt(diviation_sqr);
    	System.out.println("divition:"+diviation);
    	return expectation;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("expectation:"+get_matrix());			
	}
}
